-- MySQL dump 10.13  Distrib 8.0.23, for osx10.15 (x86_64)
--
-- Host: localhost    Database: frankatsonwarehouse
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Director','1234','password'),(2,'Emmanuel Ashie','1993','$2y$10$oHtg1O7lCtUCb5BG5dt68Ox1O1aE7LshUW3JjDyfsfWJR67oy02L.');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2019_08_19_000000_create_failed_jobs_table',1),(9,'2021_07_14_160724_create_admins_table',1),(10,'2021_07_20_104242_create_ware_houses_table',1),(12,'2021_07_22_111051_create_product_categories_table',1),(13,'2021_07_22_112645_create_products_table',1),(14,'2021_07_26_121947_create_ware_house_staff_table',1),(15,'2021_07_28_145926_create_products_warehouses_table',1),(16,'2021_08_04_012100_create_transactions_table',1),(18,'2021_07_20_124516_create_received_goods_table',2),(19,'2021_08_13_103724_create_requisitions_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `client_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0ddf4e93e9b4d67396e4a420cdca5fa13378506f269b43faf8bf2750da97250456e2ab05406d9208',1,1,'api-application','[]',0,'2021-08-16 14:42:01','2021-08-16 14:42:01','2022-08-16 14:42:01'),('5a8284de50bcc76c77d8f7e64ad46e0e4e0d41ef4d4963ae414b71a55dd016d594a7ac07952ac32a',1,1,'api-application','[]',0,'2021-08-17 12:38:16','2021-08-17 12:38:16','2022-08-17 12:38:16'),('662af1b308507ea4a7905de4b0a2080581db0275a7819448d2ac7120ab8a3e93cd6176b5b7e30137',1,1,'api-application','[]',0,'2021-09-01 12:29:16','2021-09-01 12:29:16','2022-09-01 12:29:16'),('8c910e18821b3baa2898f824309366087af501e0f5663540c008d715c3e28be1c7df611dbf73a0b0',1,1,'api-application','[]',0,'2021-08-17 07:50:55','2021-08-17 07:50:55','2022-08-17 07:50:55'),('8d24cabb18856f0442eb41fd2970751573b64e587763926e1d30fb744afda2135a477895a9e697d8',1,1,'api-application','[]',0,'2021-08-16 14:42:51','2021-08-16 14:42:51','2022-08-16 14:42:51'),('a338dfbf4f0df6bbe851f528eb41d5ec0d84181af11a693b710bb578a3e78b0440c2079d407e1480',1,1,'api-application','[]',0,'2021-09-01 11:51:00','2021-09-01 11:51:00','2022-09-01 11:51:00'),('dc43d8ce41928b86fcbee72c3d387c9a889308f8f9f865d49c2b9d416062835d510ee6e9a6b85a56',1,1,'api-application','[]',0,'2021-08-16 15:09:03','2021-08-16 15:09:03','2022-08-16 15:09:03'),('f327b8a8d4e0076c248652be5da203977e35951c32e5f703e345c3cbf6b04cde1aaa3d4cea915594',1,1,'api-application','[]',0,'2021-08-18 11:45:52','2021-08-18 11:45:52','2022-08-18 11:45:52'),('fa3cb411b173e877dc7339f5f798cad30c2fe9169aaad994cfe9ae3d31865bda391e7337c8dbf64c',1,1,'api-application','[]',0,'2021-08-17 04:17:29','2021-08-17 04:17:29','2022-08-17 04:17:29');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint unsigned NOT NULL,
  `client_id` bigint unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'kudi','3vwQO49PFIFl2fRBocxhzLDntDnL98rBIeW2zG62',NULL,'http://localhost',1,0,0,'2021-08-16 14:41:56','2021-08-16 14:41:56');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2021-08-16 14:41:56','2021-08-16 14:41:56');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_categories`
--

LOCK TABLES `product_categories` WRITE;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;
INSERT INTO `product_categories` VALUES (1,'Laprovet','2021-08-16 12:57:03','2021-08-16 12:57:03'),(2,'Kepro','2021-08-16 12:57:11','2021-08-16 12:57:11'),(3,'VMD','2021-08-16 12:57:20','2021-08-16 12:57:20');
/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint unsigned NOT NULL,
  `price` double NOT NULL,
  `pstatus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Amin Total KG','France',1,160,'active','2021-08-16 12:57:35','2021-08-16 12:57:35'),(2,'Aminovit KG','Belgium',3,140,'active','2021-08-16 12:58:14','2021-08-16 12:59:53'),(3,'Super Vitamins KG','Belgium',3,120,'active','2021-08-16 12:58:44','2021-08-16 12:59:37'),(4,'Neoxyvital Forte KG','Belgium',3,300,'active','2021-08-16 12:59:22','2021-08-16 12:59:22'),(5,'Kepromec 50ml','Holland',2,25,'active','2021-08-16 13:00:29','2021-08-16 13:00:29'),(6,'Kepromec 100ml','Holland',2,30,'active','2021-08-16 13:00:46','2021-08-16 13:00:46'),(7,'VMD Penstrep','Belgium',3,40,'active','2021-08-16 13:01:27','2021-08-16 13:01:27'),(8,'Kepro Penstrep','Holland',2,43,'active','2021-08-16 13:01:57','2021-08-16 13:01:57'),(9,'Penprovit KG','Holland',2,200,'active','2021-08-16 13:02:28','2021-08-16 13:02:28');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_warehouses`
--

DROP TABLE IF EXISTS `products_warehouses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products_warehouses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint NOT NULL,
  `ware_house_id` bigint NOT NULL,
  `quantity` int NOT NULL,
  `value` double NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_warehouses`
--

LOCK TABLES `products_warehouses` WRITE;
/*!40000 ALTER TABLE `products_warehouses` DISABLE KEYS */;
INSERT INTO `products_warehouses` VALUES (1,1,1,40,6400,'Laprovet'),(2,2,1,190,26600,'VMD'),(3,3,1,50,6000,'VMD'),(4,4,1,200,60000,'VMD'),(5,5,1,0,0,'Kepro'),(6,6,1,0,0,'Kepro'),(7,7,1,40,1600,'VMD'),(8,8,1,0,0,'Kepro'),(9,9,1,30,6000,'Kepro'),(10,9,2,0,0,'Kepro'),(11,8,2,0,0,'Kepro'),(12,7,2,0,0,'VMD'),(13,6,2,0,0,'Kepro'),(14,5,2,0,0,'Kepro'),(15,4,2,0,0,'VMD'),(16,3,2,0,0,'VMD'),(17,2,2,0,0,'VMD'),(18,1,2,0,0,'Laprovet');
/*!40000 ALTER TABLE `products_warehouses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `received_goods`
--

DROP TABLE IF EXISTS `received_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `received_goods` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `ware_house_id` int NOT NULL,
  `requisition_id` int NOT NULL,
  `quantity` int NOT NULL,
  `value` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `received_goods`
--

LOCK TABLES `received_goods` WRITE;
/*!40000 ALTER TABLE `received_goods` DISABLE KEYS */;
INSERT INTO `received_goods` VALUES (1,2,1,1,20,2800,'2021-08-17 11:11:25','2021-08-17 11:11:25'),(2,2,1,2,200,28000,'2021-08-18 11:54:45','2021-08-18 11:54:45'),(3,3,1,2,50,6000,'2021-08-18 11:54:45','2021-08-18 11:54:45'),(4,4,1,2,100,30000,'2021-08-18 11:54:45','2021-08-18 11:54:45'),(5,1,1,2,50,8000,'2021-08-18 11:54:45','2021-08-18 11:54:45'),(6,9,1,2,30,6000,'2021-08-18 11:54:45','2021-08-18 11:54:45'),(7,7,1,3,30,1200,'2021-09-01 11:56:31','2021-09-01 11:56:31'),(8,4,1,3,100,30000,'2021-09-01 11:56:31','2021-09-01 11:56:31'),(9,7,1,7,10,400,'2021-09-01 18:15:32','2021-09-01 18:15:32');
/*!40000 ALTER TABLE `received_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requisitions`
--

DROP TABLE IF EXISTS `requisitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requisitions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_id` int NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requisitions`
--

LOCK TABLES `requisitions` WRITE;
/*!40000 ALTER TABLE `requisitions` DISABLE KEYS */;
INSERT INTO `requisitions` VALUES (1,1,'Receiving goods from Kepro in Holland','complete','2021-08-17 04:38:57','2021-08-17 10:53:07'),(2,1,'Reception of goods from VMD in Belgium','complete','2021-08-18 11:43:44','2021-08-18 11:53:29'),(3,1,'Receiving goods from VMD','complete','2021-09-01 11:54:58','2021-09-01 11:55:30'),(4,1,'Receive goods from Laprovet','complete','2021-09-01 18:09:11','2021-09-01 20:10:25'),(5,1,'Receive goods from Laprovet','complete','2021-09-01 18:09:12','2021-09-01 20:10:22'),(6,1,'Receive goods from Laprovet','complete','2021-09-01 18:09:14','2021-09-01 20:10:19'),(7,1,'Receive goods from Laprovet','complete','2021-09-01 18:09:50','2021-09-01 18:11:26');
/*!40000 ALTER TABLE `requisitions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `ware_house_id` int NOT NULL,
  `quantity` int NOT NULL,
  `value` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,2,1,5,700,'2021-08-17 11:21:33','2021-08-17 11:21:33'),(2,2,1,15,2100,'2021-08-18 14:47:16','2021-08-18 14:47:16'),(3,1,1,10,1600,'2021-08-23 12:08:57','2021-08-23 12:08:57'),(4,2,1,10,1400,'2021-08-23 12:10:52','2021-08-23 12:10:52');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_pin_unique` (`pin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Director','1993','$2y$10$/yzSFHM8kMwzUsntHIuYZ.jtQQU89Wi8E4cQ.maVpf42RhBnPulXy','0','2021-08-16 14:36:55','2021-08-16 14:36:55');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ware_house_staff`
--

DROP TABLE IF EXISTS `ware_house_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ware_house_staff` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `uid` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ware_house_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ware_house_staff`
--

LOCK TABLES `ware_house_staff` WRITE;
/*!40000 ALTER TABLE `ware_house_staff` DISABLE KEYS */;
INSERT INTO `ware_house_staff` VALUES (1,'1','0','Manager','active',NULL,NULL),(2,'2','1','Manager','active','2021-08-16 11:00:37','2021-08-16 11:00:37');
/*!40000 ALTER TABLE `ware_house_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ware_houses`
--

DROP TABLE IF EXISTS `ware_houses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ware_houses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `wname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wlocation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wbranch` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wstatus` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ware_houses`
--

LOCK TABLES `ware_houses` WRITE;
/*!40000 ALTER TABLE `ware_houses` DISABLE KEYS */;
INSERT INTO `ware_houses` VALUES (1,'Accra Ware House','Orgle Road','Head Quarters','active','2021-08-16 12:29:27','2021-08-16 12:29:27'),(2,'Kumasi Ware House','Ohwimase','Kumasi Branch','active','2021-08-16 13:37:26','2021-08-16 13:37:26');
/*!40000 ALTER TABLE `ware_houses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-13 14:06:28
