@extends('layouts.dashboard')

@section('pageCss')
    <link rel="stylesheet" href={{ asset('css/stock.css') }} />
    <link rel="stylesheet" href={{ asset('css/product.css') }} />
@endsection

@section('pageMenu')
    <div class="sideMenuIconContainer" onclick="loadHome()">
        <img class="sideMenuCenterIcon" src={{ asset('icons/home2.png') }}>
        <div class="sideMenuLeftLabel">
            <label class="sideMenuLeftLabelText">Home</label>
        </div>
        <label style="display:none" id="homeURL">{{ route('homePage') }}</label>
    </div>
    @if(session()->get('role') =='Accountant')    
            <div class="sideMenuIconContainer" id="selectedMenu">
                <img class="sideMenuCenterIcon" src={{ asset('icons/stocks.png') }}>
                <div class="sideMenuLeftLabel">
                    <label class="sideMenuLeftLabelText">Stocks</label>
                </div>
                <!--<label style="display:none" id="stockURL">{{ route('stock') }}</label>-->
            </div>
    @else
            <div class="sideMenuIconContainer" onclick="loadWarehouse()">
                <img class="sideMenuCenterIcon" src={{ asset('icons/boxes.png') }}>
                <div class="sideMenuLeftLabel">
                    <label class="sideMenuLeftLabelText">Warehouse</label>
                </div>
                <label style="display:none" id="warehouseURL">{{ route('warehousePage') }}</label>
            </div>    
    @endif   
    <div class="sideMenuIconContainer" onclick="loadProducts()">
        <img class="sideMenuCenterIcon" src={{ asset('icons/product.png') }} >
        <div class="sideMenuLeftLabel">
            <label class="sideMenuLeftLabelText">Products</label>
        </div>
        <label style="display:none" id="productsURL">{{ route('productsPage') }}</label>
    </div>
    <div class="sideMenuIconContainer" onclick="loadInventory()">
        <img class="sideMenuCenterIcon" src={{ asset('icons/box2.png') }}>
        <div class="sideMenuLeftLabel">
            <label class="sideMenuLeftLabelText">Inventory</label>
        </div>
        <label style="display:none" id="inventoryURL">{{ route('inventoryPage') }}</label>
    </div>
    
    <div class="sideMenuIconContainer" onclick="loadTransactions()">
        <img class="sideMenuCenterIcon" src={{ asset('icons/transaction.png') }}>
        <div class="sideMenuLeftLabel">
            <label class="sideMenuLeftLabelText">Transactions</label>
        </div>
        <label style="display:none" id="transactionURL">{{ route('transactionPage') }}</label>
    </div>
    
    <div class="sideMenuIconContainer" onclick="loadProformer()">
        <img class="sideMenuCenterIcon" src={{ asset('icons/invoice.png') }} >
        <div class="sideMenuLeftLabel">
            <label class="sideMenuLeftLabelText">Proformer</label>
        </div>
        <label style="display:none" id="proformerURL">{{ route('profermerPage') }}</label>
    </div>
    <div class="sideMenuIconContainer" onclick="showLogOut()">
        <div class="sideMenuLeftLabel">
            <label class="sideMenuLeftLabelText">Logout</label>
        </div>
        <img class="sideMenuCenterIcon" src={{ asset('icons/logout.png') }}>
    </div>
 
@endsection

@section('pageJs')
 <script>
     var totalOldValue = 0,totalNewValue = 0,differenceValue=0,differenceQuantity=0;
     var productNames = [],previousQuantity = [],currentQuantity=[],quantityDifference=[];
     var previousValue = [],currentValue = [], valueDifference = [];
     var stockDate = "";
     function processStock(){
       alert("Hello");
     }

     function getStockData(){
        var stockRequest  = $.ajax({
                                url:"/stockData",
                                method: "GET",
                                data:{
                                    "_token": "{{ csrf_token() }}"
                                }
        });
        stockRequest.done(function(response, textStatus, jqXHR){
            console.log("Response is: "+response);                                       
        });
        
     }

     function printStock(){
        var today = new Date();
            var month = today.getMonth()+1;
            var format = today.getDate()+" "+getCurrentDate(month)+" "+today.getFullYear();
            var wName = "{{ $warehouse->wname }}";
            var wLocation = "{{ $warehouse->wlocation }}"
            var printWindow = window.open('', '', 'height=800,width=1200');  
            printWindow.document.write('<html><head><title>Inventory</title>');  
            printWindow.document.write('</head>'+
            '<style>'+
            ' table, th, td {'+
            'border: 1px solid black; text-align:center}'+
            '#historyTable{'+
            'left:5%;width:90%;height:auto;position:absolute;'+
            '}'+
            '</style>'+
            '<body>');  
            printWindow.document.write('<div style="width:100%;height:auto;padding-top:5px;padding-bottom:5px;text-align:center;">'
            +'<h2>Stock Report<h2>'+
            '</div>'+
            '<div style="width:100%;height:auto;top:45px;text-align:center;position:absolute">'+
            '<h3>'+wName+'('+wLocation+')</h3>'+
            '<div style="width:100%;height:auto;top:40px;text-align:center;position:absolute">'
            +'<h3>'+stockDate+'</h3>'+
            '</div>'+
            '<div style="top:100px;width:100%;height:auto;position:absolute;">'+
                '<table id="historyTable">'+
                '<thead>'+
                '<tr>'+
                '<th>Product Name</th>'+
                '<th>Previous Quantity</th>'+
                '<th>Current Quantity</th>'+
                '<th>Quantity Difference</th>'+
                '<th>Previous Value</th>'+
                '<th>Current Value</th>'+
                '<th>Value Difference</th>'+
                '</tr>'+
                '</thead>'+
                '<tbody id="historyTableBody">');
                var totalQuantity = 0,totalValue = 0;
                for(var i=0;i<productNames.length;i++){
                    printWindow.document.write(
                    '<tr>'+
                        '<td>'+productNames[i]+'</td>'+
                        '<td>'+previousQuantity[i]+'</td>'+
                        '<td>'+currentQuantity[i]+'</td>'+
                        '<td>'+quantityDifference[i]+'</td>'+
                        '<td> &#8373 '+previousValue[i]+'</td>'+
                        '<td> &#8373 '+currentValue[i]+'</td>'+
                        '<td> &#8373 '+valueDifference[i]+'</td>'+
                    '</tr>'
                    );
                    console.log(previousValue[i]);
                    //totalQuantity = totalQuantity + parseInt(productQuantities[i]);
                    //totalValue = totalValue + parseFloat(productValue[i]);
                }
                printWindow.document.write(
                    '<tr>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td>Total</td>'+
                        '<td style="color:black;font-weight: 900;">'+differenceQuantity+'</td>'+
                        '<td style="color:black;font-weight: 900;"> &#8373 '+totalOldValue+'</td>'+
                        '<td style="color:black;font-weight: 900;"> &#8373 '+totalNewValue+'</td>'+
                        '<td style="color:black;font-weight: 900;"> &#8373 '+differenceValue+'</td>'+
                    '</tr>'
                );

                printWindow.document.write(
                    '</tbody>'+
                    '</table>'+
                    '</div>'
                );
            
            printWindow.document.write('</body></html>');  
            printWindow.document.close();  
            printWindow.print(); 
     }

     function processStock(){
         var stockId = "{{ $stockID }}";
        Swal.fire({
            title: 'Confirm Stock',
            text: "Are you certain with all the stock figures displayed below?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0093E9',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Proceed'
            }).then((result) => {  
                if (result.value) {
                var categoryDeleteRequest  = $.ajax({
                        url:"/submitStock",
                        method: "GET",
                        data:{
                            stockId:stockId,
                            "_token": "{{ csrf_token() }}"
                        }
                });
                categoryDeleteRequest.done(function(response, textStatus, jqXHR){
                    
                    if(response == "Success"){
                    Swal.fire({
                                    title: 'Successfully confirmed stock details',
                                    text: "",
                                    icon: 'success',
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Okay'
                                }).then((result) => {
                                    if (result.value) {
                                        location.reload();
                                        //$("#productCategoryTableBody").empty();
                                        // getProductCategories();
                                    
                                    }
                            });
                    }
                    else{
                    showError("Category Deletion Failed",response);
                    }
                });
                }
        });
     }
 </script>
@endsection

@section('pageContent')
   @isset($date)
      <script>
          var unProcessedStockDate = "{{ $date }}";
          var dateExtract = unProcessedStockDate.substring(0,10);
          //alert(dateExtract);
          var month = dateExtract.substring(5,7);
          var day = dateExtract.substring(8,10);
          var year = dateExtract.substring(0,4);

          stockDate = ""+day+"/"+getCurrentDate(month)+"/"+year;
          
      </script>
   @endisset
   <div id="productBody">
       <div class="productHead">
          <label class="productTitle">Stock Manager</label>
          <div class="addBtnCon" onclick="processStock()">
            <img class="addBtnConIcon" src="/icons/process.png">
            <label class="addBtnConLable">Confirm Stock</label>
          </div>
          <div id="printStockCon" onclick="printStock()">
            <img class="addBtnConIcon" src="/icons/white_print.png">
            <label class="addBtnConLable">Print Stock</label>
          </div>
          <div id='stockStatusCon'>
             <label id="stockStatus">Hi</label>
          </div>
          <script>
            var status = "{{ $status }}";
            if(status == "pending"){
               document.getElementById('stockStatus').innerHTML = "Pending Confirmation"
            }
            else{
                document.getElementById('stockStatus').innerHTML = "Awaiting Approval"
            }
         </script>
       </div>
       <div class="productContent">
            <div class="tableContainer">
                <table id="stocksTable">
                    <thead>
                        <tr>
                        <th>Product</th>
                        <th>Previous Quantity</th>
                        <th>Current Quantity</th>
                        <th>Quantity Difference</th>
                        <th>Previous Value</th>
                        <th>Current Value</th>
                        <th>Value Difference</th>
                        </tr> 
                    </thead> 
                    <tbody id="tbody2">
                        @foreach ($stockData as $row2)
                            <tr>
                                <td>{{ $row2->products->name }}</td>
                                <td>{{ $row2->old_quantity }}</td>
                                <td>{{ $row2->new_quantity }}</td>
                                <td>{{ $row2->difference_quantity }}</td>
                                <td>&#8373 {{ $row2->old_value }}</td>
                                <td>&#8373 {{ $row2->new_value }}</td>
                                <td>&#8373 {{ $row2->difference_value }}</td>
                            </tr>
                            <script>
                                var oValue = "{{ $row2->old_value }}";
                                var nValue = "{{ $row2->new_value }}";
                                var dValue = "{{ $row2->difference_value }}";
                                var qValue = "{{ $row2->difference_quantity }}";
                                
                                productNames.push("{{ $row2->products->name }}");
                                previousQuantity.push("{{ $row2->old_quantity }}");
                                currentQuantity.push("{{ $row2->new_quantity }}");
                                quantityDifference.push("{{ $row2->difference_quantity }}");
                                previousValue.push("{{ $row2->old_value }}");
                                currentValue.push("{{ $row2->new_value }}");
                                valueDifference.push("{{ $row2->difference_value }}");


         
                                totalOldValue = totalOldValue + parseInt(oValue);
                                totalNewValue = totalNewValue + parseInt(nValue);
                                differenceValue = differenceValue + parseInt(dValue);
                                differenceQuantity = differenceQuantity + parseInt(qValue);

                            </script>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="totalbox">
                <div id="box1">
                   <label class="boxheading">Total Old Value</label>
                   <label class="boxValue" id="totalOldValue"></label>
                   <div class="rightDivider"></div>
                </div>
                <div id="box2">
                   <label class="boxheading">Total New Value</label>
                   <label class="boxValue" id="totalNewValue"></label>
                   <div class="rightDivider"></div>
                </div>
                <div id="box3">
                    <label class="boxheading">Difference In Quantity</label>
                    <label class="boxValue" id="differenceQuantity"></label>
                   <div class="rightDivider"></div>
                </div>
                <div id="box4">
                    <label class="boxheading">Difference In Value</label>
                    <label class="boxValue" id="differenceValue"></label>
                </div>
                <div id="topDivider"></div>
                <script>

                    document.getElementById('totalOldValue').innerHTML = "&#8373 "+totalOldValue;
                    document.getElementById('totalNewValue').innerHTML = "&#8373 "+totalNewValue;
                    document.getElementById('differenceValue').innerHTML = "&#8373 "+differenceValue;
                    document.getElementById('differenceQuantity').innerHTML = ""+ differenceQuantity;
                    console.log("Total Old Value: "+totalOldValue);
                    
                </script>
            </div>
       </div>
   </div>

@endsection
